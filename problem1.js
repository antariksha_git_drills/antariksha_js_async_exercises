import * as fs from "node:fs";
import { writeFile, unlink } from "node:fs/promises";

function fsApproach(directory) {
  fs.writeFile(directory + "/File1.json", '{"Name":"A"}', (err) => {
    if (err) {
      console.log("Error in creating File1 using fs");
    } else {
      console.log("File1 created using fs");
      fs.writeFile(directory + "/File2.json", '{"Name":"B"}', (err) => {
        if (err) {
          console.log("Error in creating File2 using fs");
        } else {
          console.log("File2 created using fs");
          fs.writeFile(directory + "/File3.json", '{"Name":"C"}', (err) => {
            if (err) {
              console.log("Error in creating File3 using fs");
            } else {
              console.log("File3 created using fs");
              fs.unlink(directory + "/File1.json", (err) => {
                if (err) {
                  console.log("Error in deleting File1 using fs");
                } else {
                  console.log("File1 deleted using fs");
                  fs.unlink(directory + "/File2.json", (err) => {
                    if (err) {
                      console.log("Error in deleting File2 using fs");
                    } else {
                      console.log("File2 deleted using fs");
                      fs.unlink(directory + "/File3.json", (err) => {
                        if (err) {
                          console.log("Error in deleting File3 using fs");
                        } else {
                          console.log("File3 deleted using fs");
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function promisesApproach(directory) {
  Promise.all([
    writeFile(directory + "/File1.json", '{"Name":"A"}'),
    console.log("File1 using promise created"),
    writeFile(directory + "/File2.json", '{"Name":"B"}'),
    console.log("File2 using promise created"),
    writeFile(directory + "/File3.json", '{"Name":"C"}'),
    console.log("File2 using promise created"),
  ])
    .then(() => {
      unlink(directory + "/File1.json"),
        console.log("File1 using promise deleted"),
        unlink(directory + "/File2.json"),
        console.log("File2 using promise deleted"),
        unlink(directory + "/File3.json"),
        console.log("File3 using promise deleted");
    })
    .catch((err) => {
      console.log(err.message);
    });
}

async function asyncApproachCreate(directory) {
  await fs.writeFile(directory + "/File1.json", '"Name":"A"', (err) => {
    if (err) {
      console.log("Error in creating File1 using async");
    } else {
      console.log("File1 created using async");
    }
  });
  await fs.writeFile(directory + "/File2.json", '"Name":"B"', (err) => {
    if (err) {
      console.log("Error in creating File2 using async");
    } else {
      console.log("File2 created using async");
    }
  });
  await fs.writeFile(directory + "/File3.json", '"Name":"C"', (err) => {
    if (err) {
      console.log("Error in creating File3 using async");
    } else {
      console.log("File3 created using async");
      asyncApproachDelete(directory);
    }
  });
  return 1;
}

async function asyncApproachDelete(directory) {
  await fs.unlink(directory + "/File1.json", (err) => {
    if (err) {
      console.log("Error in deleting File1 using async");
    } else {
      console.log("File1 deleted using async");
    }
  });
  await fs.unlink(directory + "/File2.json", (err) => {
    if (err) {
      console.log("Error in deleting File2 using async");
    } else {
      console.log("File2 deleted using async");
    }
  });
  await fs.unlink(directory + "/File3.json", (err) => {
    if (err) {
      console.log("Error in deleting File3 using async");
    } else {
      console.log("File3 deleted using async");
    }
  });
}

export const part1 = fsApproach;
export const part2 = promisesApproach;
export const part3 = asyncApproachCreate;
export const part4 = asyncApproachDelete;
