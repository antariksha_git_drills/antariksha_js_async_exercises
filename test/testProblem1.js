import { part1, part2, part3, /*part4*/} from '../problem1.js';
import * as fs from 'node:fs';

const directory1='fsApproach';
const directory2='promisesApproach';
const directory3='asyncApproach';

setTimeout(() =>{
	fs.mkdir(directory1,function(err){
		if(err){
			console.error('Directory using fs module Already Exists');
		}
		else{
			console.log('Directory using fs module created');
		}
		part1(directory1);
	});
	setTimeout(() =>{
		fs.mkdir(directory2,function(err){
			if(err){
				console.error('Directory using promise Already Exists');
			}
			else{
				console.log('Directory using promise created');
			}
			part2(directory2);
		});
		setTimeout(() =>{
			fs.mkdir(directory3,function(err){
				if(err){
					console.error('Directory using asyncApproach Already Exists');
				}
				else{
					console.log('Directory using asyncApproach created');
				}
				part3(directory3);
			});
		},1000);
	},1000);
},1000);

/*fs.mkdir(directory1,function(err){
	if(err){
		console.error('Directory using fs module Already Exists');
	}
	else{
		console.log('Directory using fs module created');
	}
	part1(directory1);
});
fs.mkdir(directory2,function(err){
	if(err){
		console.error('Directory using promise Already Exists');
	}
	else{
		console.log('Directory using promise created');
	}
	part2(directory2);
});

fs.mkdir(directory3,function(err){
	if(err){
		console.error('Directory using promise Already Exists');
	}
	else{
		console.log('Directory using promise created');
	}
	part3(directory3);
});
*/
//const sampleFile=['File1','File2','File3'];

//const sampleData={'Name':'A','City':'B','Food':'C'};

