//import * as fs from 'node:fs';
/*import {
  read,
  writeUpper,
  writeForEachSentence,
  sortText,
  deleteText,
} from '../problem2.js';
import * as fs from 'node:fs';

async function main() {
  let f1 = '../lipsum.txt';
  let f2 = './upperCase.txt';
  let f3 = './fileName.txt';
  let f4 = 'sorted.txt';
  await fs.promises.mkdir('./Files');
  let a=await read(f1);
  console.log(a);
  await writeUpper(f1, f2);
  await writeForEachSentence(f2, f3);
  await sortText(f3, f4);
  await deleteText(f3);
}

main();*/
import * as fs from "node:fs";
//const fs = require("fs");

function readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        // console.log(data);
        resolve(data);
      }
    });
  });
}

function writeFile(path, content) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, content, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        // console.log(data);
        resolve(path);
      }
    });
  });
}

function appendNewLineToFile(path, line) {
  return new Promise((resolve, reject) => {
    const content = `${line.trim()}\n`;
    fs.appendFile(path, content, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(content);
      }
    });
  });
}

function deleteFiles(path) {
  return new Promise((resolve, reject) => {
    fs.unlink(path, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(path);
      }
    });
  });
}
// functions -> do one thing and do it well

async function main() {
  try {
    const data = await readFile("../lipsum.txt");
    const dataUpper = data.toUpperCase();
    // console.log(dataUpper);
    await writeFile("uppercase.txt", dataUpper);
    console.log("dataUpper written to uppercase.txt");
    await appendNewLineToFile("filenames.txt", "uppercase.txt");
    console.log("uppercase.txt appended to filenames.txt");
    const uppercaseDataReadFromFile = await readFile("./uppercase.txt");
    const lowercaseData = uppercaseDataReadFromFile.toLowerCase();
    console.log(lowercaseData);
    const sentences = lowercaseData.split(". ");
    //console.log(sentences);
    const sentencesFilesPromises = sentences.map((sentence, index) => {
      return writeFile(`./data/${index}.txt`, sentence);
    });
    const sentenceFilePaths = await Promise.all(sentencesFilesPromises);
    console.log(sentenceFilePaths);
    await sentenceFilePaths.map((path) => {
      return appendNewLineToFile("filenames.txt", path);
    });
    console.log("sentence files appended to filenames.txt");
    const sortedSentence = sentences.sort();
    console.log(sortedSentence);
    let sortedContent = "";
    for (let index = 1; index < sortedSentence.length; index++) {
      sortedContent += sortedSentence[index] + ". ";
    }
    console.log(sortedContent);
    await appendNewLineToFile("sort.txt", sortedContent);
    console.log("Sorted content written in ");
    await appendNewLineToFile("filenames.txt", "sort.txt");
    console.log("sot.txt appended to filenames.txt");
    await sentenceFilePaths.map((path) => {
      return deleteFiles(path);
    });
    await deleteFiles("uppercase.txt");
    console.log("uppercase.txt deleted");
    await deleteFiles("sort.txt");
    console.log("sort.txt deleted");
  } catch (e) {
    console.error("Error: ", e);
  }
}
main();
