import * as fs from 'node:fs';

async function readFile(f1) {
	return(fs.promises.readFile(f1, 'utf-8'));
}

async function writeFileInUpperCase(f1,f2){
	try{
		let data= await readFile(f1);
		await fs.promises.writeFile(f2,data.toUpperCase());
	}
	catch(err){
		console.log(err);
	} 
}

async function writeFileForEachSentence(f1,f2){
	try{
		let data= await readFile(f1);
		let inLowerCase=data.toLowerCase();
		let contentsplit1=inLowerCase.replace(/(\n)/gm, '');
		//console.log(contentsplit1);
		let contentsplit=contentsplit1.split('. ');
		let index=1;
		let fileName='';
		for(let line of contentsplit){
			await fs.promises.writeFile(`./Files/File${index}.txt`,line);
			fileName+=` File${index}.txt`;
			index++;
		}
		await fs.promises.writeFile(f2,fileName);
	}
	catch(err){
		console.log(err);
	}
}

async function sortContent(f1,f2){
	try{
		let fileName=await readFile(f1);
		let files=fileName.split(' ');
		//console.log(files);
		let data=[];
		for(let index=1;index<files.length;index++){
			data[index-1]=await readFile(`./Files/${files[index]}`);
		}
		data.sort();
		let newContent='';
		for(let index=1;index<data.length;index++){
			newContent+=data[index]+'. ';
		}
		//console.log(newContent);
		await fs.promises.writeFile(`./Files/${f2}`,newContent);
		fileName+=` ${f2}`;
		await fs.promises.writeFile(f1,fileName);
	}
	catch(err){
		console.log(err);
	}
}

async function deleteContent(f1){
	try{
		let fileName=await readFile(f1);
		let files=fileName.split(' ');
		//console.log(files);
		for(let index=1;index<files.length;index++){
			await fs.promises.unlink(`./Files/${files[index]}`);
		}
	}
	catch(err){
		console.log(err);
	}
}
export const read = readFile;
export const writeUpper = writeFileInUpperCase;
export const writeForEachSentence = writeFileForEachSentence;
export const sortText = sortContent;
export const deleteText=deleteContent;
